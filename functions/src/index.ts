import * as functions from 'firebase-functions';
import * as express from 'express';
//const admin = require('firebase-admin');

const cheerio = require('cheerio');
const fetch = require('node-fetch');

const app = express();

const FetchBagNews = async () => {
  const res = await fetch('https://www.bag.admin.ch/bag/de/home/das-bag/aktuell/news.html');

  const html = await res.text();
  const $ = cheerio.load(html);

  let news = <any>[];
  console.log('found a total of: ' + $('.mod.mod-teaser').length + ' News');

  $('.mod.mod-teaser').each((index: number, item: any) => {
    let nn = {
      image: 'https://www.bag.admin.ch' + $('img', item).attr('src'),
      title: $('a', item).html(),
      url: 'https://www.bag.admin.ch' + $('a', item).attr('href'),
      id: $('a', item).attr('href'),
      summary: $('.wrapper p', item).html(),
      type: 'Article'
    };
    news.push(nn);
  });

  return news
}

app.get('/bagNews', async (request, response) => {
  const data = await FetchBagNews();
  response.set('Access-Control-Allow-Origin', '*');
  response.send(data);
});

/*
app.get('/stats', functions.https.onRequest((request, response) => {
  db.collection("users").get().then((querySnapshot : any) => {
    console.log("total users" , querySnapshot.size);
  });

  db.collection('users')
    .where('riskModel.riskType', '==', 'danger').get().then((res: any) => {
    console.log("Total danger: ", res);
  })
  response.send("caca")
}));
*/

// app.listen(3000, () => console.log(`Example app listening at http://localhost:${port}`))
const opts: functions.RuntimeOptions = {
  memory: '2GB',
  timeoutSeconds: 60
}
export const api = functions.runWith(opts).https.onRequest(app);
