**[Checkout the Product Page](https://stayathomelandingpage.now.sh)**

**[Try the app in your phone](https://stayathome-chi.now.sh/)**

## Inspiration
Hello, my name is Nicolas Cepeda. In the last 3 weeks almost my entire Family was in contact with the corona virus (total of 5 out of 6 members) with different severity levels (mild till hospitalisation).

The Corona threat is real (and needs to handled with respect), is up to all of us to save lives.

## What does the app do?

The #LetsStayAtHome App is designed to help people:
- Manage their quarantine
- Based on all the data collected calculate the likelihood that the person has been exposed to the virus
- Inform about the availability of close by hospitals
- Spread official news and help detect fake news
- How to act in case of Emergency

[Try the app in your phone](https://stayathome-chi.now.sh/)

## Running the code

Clone the app repository

```
cd app
npm install
npm run start
```
##Risk Modelling

### Data Collection
The App collects following data at onboarding:
- General data (age, kanton, etc)
- Past Health Conditions relevant to Covid
- Questions regarding the quality of the quarantine
- Symptom check

And prompts the user everyday to do a self check containing following information:
- Symptom check
- Questions regarding the quality of the quarantine

### Algorithm
With all this data the app calculates the likelihood that the user as been exposed to the virus and generates a recommendation (stay at home, etc).

#### Current Implementation
The current model is based on simple linear algebra and needs to be recalibrated.

#### ML-based implementation
The anonymized data from the users are being collected in a centralised fashion.

Once enough data has been collected (specially interesting are users that became COVID+ or show symptoms of infection) an ML model is going to be trained to better predict the likelihood of infection.

### Resources:

- App [https://stayathome-chi.now.sh/](https://stayathome-chi.now.sh/)
- Slides: [https://docs.google.com/presentation/d/1FSCYgUBLNA0WpQUyPhGrY3s7z-ccpaeFFv6y1i_MFMg/edit?usp=sharing](https://docs.google.com/presentation/d/1FSCYgUBLNA0WpQUyPhGrY3s7z-ccpaeFFv6y1i_MFMg/edit?usp=sharing)
- Source Code App [https://bitbucket.org/cepedanicolas/stayathome/src](https://bitbucket.org/cepedanicolas/stayathome/src)
- Source Code Landing Page [https://bitbucket.org/cepedanicolas/stayathomelandingpage/src](https://bitbucket.org/cepedanicolas/stayathomelandingpage/src)

- 2 Minutes Video Pitch [https://www.youtube.com/watch?v=zQwjiFWyZH0&feature=youtu.be](https://www.youtube.com/watch?v=zQwjiFWyZH0&feature=youtu.be)

## Problems to Solve

The app focuses in the following problems:

- **Quarantine management** Help manage self quarantine better.
- **Hospital** Help triage for more efficient resources management.
- **Economic** Help reduce the economic impact.
- **Information** Help spread official information and protect from Fake News
- **Data** Anonymized Data Collection for better projection and models

## Product Roadmap


### **Current - v0.7-beta**

[Try this version on your phone](https://stayathome-chi.now.sh/)

- Onboarding
- Symptom Checkup
- Simple Risk Modelling
- Daily BAG News & Recommendations
- Help in case of Emergency
- Fake News Detection
- Review all text & models
- Improved Risk Modelling
- Anonymised Data Collection
- Landing Page
- I18N

### **Next - v0.7-alpha** 

App Store Release required

- Push Notifications
- Push Notifications
- Google Maps integration
- Canton Overview: Show by Kanton the percentage of people that are green. (Charting Technology from https://devpost.com/software/covid19-live-tracker)
- Bluetooth Usage to detected proximity to other people (Seems this project is already doing this: https://devpost.com/software/80_corona-contact-tracker_coronow).
- QR-Code based immunity certificate (through scanning of a QR code the person can prove he is Corona Immune and is allowed to go out.)


## Contributors

Following Persons have contributed to this project:


- Margaux Revel [Linkedin](https://www.linkedin.com/in/margauxrevel/)
- Florian Westermann [Linkedin](https://www.linkedin.com/in/florianwestermann/)
- Julia Ceberos [Linkedin](https://www.linkedin.com/in/julia-cebreros/)
- Nicolas Cepeda [Linkedin](https://www.linkedin.com/in/nicolas-c-a0195a2b/)[Twitter](https://twitter.com/cepedanicolas)

## Feedback

Feedback of any kind is highly welcome at [cepeda.nicolas@gmail.com](mailto:cepeda.nicolas@gmail.com).