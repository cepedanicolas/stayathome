/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */
import { HTMLStencilElement, JSXBase } from "@stencil/core/internal";
import { News, Symptoms, UserData, } from "./service/model";
export namespace Components {
    interface AppDailyCheck {
    }
    interface AppFakeNews {
    }
    interface AppHome {
    }
    interface AppInfos {
    }
    interface AppNews {
        "newsId": string;
        "newsType": string;
    }
    interface AppOnboarding {
        "data0ButtonEnabled": boolean;
        "data1ButtonEnabled": boolean;
        "data3ButtonEnabled": boolean;
        "state": string;
    }
    interface AppRoot {
    }
    interface AppSos {
    }
    interface AppTabs {
    }
    interface CircleProgress {
        "progressAmount": string;
        "progressColor": string;
    }
    interface NewsItem {
        "news": News;
        "size": string;
    }
    interface StatusUser {
        "userData": UserData;
    }
    interface SymptomCheck {
        "dailyCheck": boolean;
        "symptoms": Symptoms;
    }
}
declare global {
    interface HTMLAppDailyCheckElement extends Components.AppDailyCheck, HTMLStencilElement {
    }
    var HTMLAppDailyCheckElement: {
        prototype: HTMLAppDailyCheckElement;
        new (): HTMLAppDailyCheckElement;
    };
    interface HTMLAppFakeNewsElement extends Components.AppFakeNews, HTMLStencilElement {
    }
    var HTMLAppFakeNewsElement: {
        prototype: HTMLAppFakeNewsElement;
        new (): HTMLAppFakeNewsElement;
    };
    interface HTMLAppHomeElement extends Components.AppHome, HTMLStencilElement {
    }
    var HTMLAppHomeElement: {
        prototype: HTMLAppHomeElement;
        new (): HTMLAppHomeElement;
    };
    interface HTMLAppInfosElement extends Components.AppInfos, HTMLStencilElement {
    }
    var HTMLAppInfosElement: {
        prototype: HTMLAppInfosElement;
        new (): HTMLAppInfosElement;
    };
    interface HTMLAppNewsElement extends Components.AppNews, HTMLStencilElement {
    }
    var HTMLAppNewsElement: {
        prototype: HTMLAppNewsElement;
        new (): HTMLAppNewsElement;
    };
    interface HTMLAppOnboardingElement extends Components.AppOnboarding, HTMLStencilElement {
    }
    var HTMLAppOnboardingElement: {
        prototype: HTMLAppOnboardingElement;
        new (): HTMLAppOnboardingElement;
    };
    interface HTMLAppRootElement extends Components.AppRoot, HTMLStencilElement {
    }
    var HTMLAppRootElement: {
        prototype: HTMLAppRootElement;
        new (): HTMLAppRootElement;
    };
    interface HTMLAppSosElement extends Components.AppSos, HTMLStencilElement {
    }
    var HTMLAppSosElement: {
        prototype: HTMLAppSosElement;
        new (): HTMLAppSosElement;
    };
    interface HTMLAppTabsElement extends Components.AppTabs, HTMLStencilElement {
    }
    var HTMLAppTabsElement: {
        prototype: HTMLAppTabsElement;
        new (): HTMLAppTabsElement;
    };
    interface HTMLCircleProgressElement extends Components.CircleProgress, HTMLStencilElement {
    }
    var HTMLCircleProgressElement: {
        prototype: HTMLCircleProgressElement;
        new (): HTMLCircleProgressElement;
    };
    interface HTMLNewsItemElement extends Components.NewsItem, HTMLStencilElement {
    }
    var HTMLNewsItemElement: {
        prototype: HTMLNewsItemElement;
        new (): HTMLNewsItemElement;
    };
    interface HTMLStatusUserElement extends Components.StatusUser, HTMLStencilElement {
    }
    var HTMLStatusUserElement: {
        prototype: HTMLStatusUserElement;
        new (): HTMLStatusUserElement;
    };
    interface HTMLSymptomCheckElement extends Components.SymptomCheck, HTMLStencilElement {
    }
    var HTMLSymptomCheckElement: {
        prototype: HTMLSymptomCheckElement;
        new (): HTMLSymptomCheckElement;
    };
    interface HTMLElementTagNameMap {
        "app-daily-check": HTMLAppDailyCheckElement;
        "app-fake-news": HTMLAppFakeNewsElement;
        "app-home": HTMLAppHomeElement;
        "app-infos": HTMLAppInfosElement;
        "app-news": HTMLAppNewsElement;
        "app-onboarding": HTMLAppOnboardingElement;
        "app-root": HTMLAppRootElement;
        "app-sos": HTMLAppSosElement;
        "app-tabs": HTMLAppTabsElement;
        "circle-progress": HTMLCircleProgressElement;
        "news-item": HTMLNewsItemElement;
        "status-user": HTMLStatusUserElement;
        "symptom-check": HTMLSymptomCheckElement;
    }
}
declare namespace LocalJSX {
    interface AppDailyCheck {
    }
    interface AppFakeNews {
    }
    interface AppHome {
    }
    interface AppInfos {
    }
    interface AppNews {
        "newsId"?: string;
        "newsType"?: string;
    }
    interface AppOnboarding {
        "data0ButtonEnabled"?: boolean;
        "data1ButtonEnabled"?: boolean;
        "data3ButtonEnabled"?: boolean;
        "state"?: string;
    }
    interface AppRoot {
    }
    interface AppSos {
    }
    interface AppTabs {
    }
    interface CircleProgress {
        "progressAmount"?: string;
        "progressColor"?: string;
    }
    interface NewsItem {
        "news"?: News;
        "size"?: string;
    }
    interface StatusUser {
        "userData"?: UserData;
    }
    interface SymptomCheck {
        "dailyCheck"?: boolean;
        "symptoms"?: Symptoms;
    }
    interface IntrinsicElements {
        "app-daily-check": AppDailyCheck;
        "app-fake-news": AppFakeNews;
        "app-home": AppHome;
        "app-infos": AppInfos;
        "app-news": AppNews;
        "app-onboarding": AppOnboarding;
        "app-root": AppRoot;
        "app-sos": AppSos;
        "app-tabs": AppTabs;
        "circle-progress": CircleProgress;
        "news-item": NewsItem;
        "status-user": StatusUser;
        "symptom-check": SymptomCheck;
    }
}
export { LocalJSX as JSX };
declare module "@stencil/core" {
    export namespace JSX {
        interface IntrinsicElements {
            "app-daily-check": LocalJSX.AppDailyCheck & JSXBase.HTMLAttributes<HTMLAppDailyCheckElement>;
            "app-fake-news": LocalJSX.AppFakeNews & JSXBase.HTMLAttributes<HTMLAppFakeNewsElement>;
            "app-home": LocalJSX.AppHome & JSXBase.HTMLAttributes<HTMLAppHomeElement>;
            "app-infos": LocalJSX.AppInfos & JSXBase.HTMLAttributes<HTMLAppInfosElement>;
            "app-news": LocalJSX.AppNews & JSXBase.HTMLAttributes<HTMLAppNewsElement>;
            "app-onboarding": LocalJSX.AppOnboarding & JSXBase.HTMLAttributes<HTMLAppOnboardingElement>;
            "app-root": LocalJSX.AppRoot & JSXBase.HTMLAttributes<HTMLAppRootElement>;
            "app-sos": LocalJSX.AppSos & JSXBase.HTMLAttributes<HTMLAppSosElement>;
            "app-tabs": LocalJSX.AppTabs & JSXBase.HTMLAttributes<HTMLAppTabsElement>;
            "circle-progress": LocalJSX.CircleProgress & JSXBase.HTMLAttributes<HTMLCircleProgressElement>;
            "news-item": LocalJSX.NewsItem & JSXBase.HTMLAttributes<HTMLNewsItemElement>;
            "status-user": LocalJSX.StatusUser & JSXBase.HTMLAttributes<HTMLStatusUserElement>;
            "symptom-check": LocalJSX.SymptomCheck & JSXBase.HTMLAttributes<HTMLSymptomCheckElement>;
        }
    }
}
