import {Component, Element, h} from "@stencil/core";
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: "app-root",
  styleUrl: "app-root.scss"
})
export class AppRoot {
  @Element() element: HTMLElement;
  translations: any;

  async componentWillLoad() {
    this.translations = await loadI18N(this.element);
  }

  render() {
    return [
      <ion-router useHash={false}>
        <ion-route component="app-tabs">
          <ion-route url="/home" component="tab-home">
            <ion-route component="app-home"></ion-route>
          </ion-route>
          <ion-route url="/infos" component="tab-infos">
            <ion-route component="app-infos"></ion-route>
          </ion-route>
          <ion-route url="/sos" component="tab-sos">
            <ion-route component="app-sos"></ion-route>
          </ion-route>
          <ion-route url="/fake-news" component="tab-fake-news">
            <ion-route component="app-fake-news"></ion-route>
          </ion-route>
        </ion-route>

        <ion-route url="/" component="app-onboarding"></ion-route>
        <ion-route url="/news/:newsId/:newsType" component="app-news"></ion-route>
        <ion-route url="/onboarding/:state" component="app-onboarding"></ion-route>
        <ion-route url="/daily-check" component="app-daily-check"></ion-route>
      </ion-router>,
      < ion-nav/>
    ];
  }
}
