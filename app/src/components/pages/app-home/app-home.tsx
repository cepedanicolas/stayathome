import {Component, Element, h, Listen, State} from '@stencil/core';
import {dataService} from "../../../service/data-service";
import {GlobalData, News, UserData} from "../../../service/model";
import {loadI18N} from "../../../helpers/locale";
import {newsService} from "../../../service/news-service";

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {
  @State() userData: UserData;
  @State() globalData: GlobalData;
  @State() latestNews: News[] = [];

  @Element() element: HTMLElement;

  deferredPrompt;
  translations : any;

  @State() installButton : boolean = false;

  async componentWillLoad() {
    console.log("app home initialize");
    this.translations = await loadI18N(this.element);

    this.userData = await dataService.getUserData();
    this.userData = await dataService.recalculate(this.userData);

    this.globalData = await dataService.getGlobalData();
    this.latestNews = await newsService.getLastestNews(3);
    console.log("finish");
    this.setupHomeButton();
  }

  setupHomeButton(){
    window.addEventListener('beforeinstallprompt', (e) => {
      console.log("before install" ,e);
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later on the button event.
      this.deferredPrompt = e;

      // Update UI by showing a button to notify the user they can add to home screen
      this.installButton = true;
    });


    window.addEventListener('appinstalled', (event) => {
      console.log("installed ", event);
      this.installButton = false;
    });


    if (window.matchMedia('(display-mode: standalone)').matches) {
      console.log("Standalone Mode");
    }
  }

  installApp(event){
    console.log("Install App: ", this.deferredPrompt, "event: ", event);
    if(!this.deferredPrompt){
      // NO prompt to deferr
      return;
    }
    // hide our user interface that shows our button
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          this.installButton = false;
        } else {
        }
        this.deferredPrompt = null;
      });
  };

  @Listen('ionRefresh', {target: 'window'})
  async ionRefresh() {
    console.log("Refreshing");
    await newsService.emptyNewsCache();

    this.globalData = await dataService.getGlobalData();
    this.latestNews = await newsService.getLastestNews(3);
    document.querySelector("ion-refresher").complete();
    console.log("Finish refreshing");
  }

  render() {
    if(!this.userData.basicData.onboarding){
      // Redirect to onboarding
      console.log("Redirecting to onboarding from HOME");
      document.querySelector("ion-router").push('/');
      return;
    }

    return [
      <ion-header translucent={true}>
        <ion-toolbar>
          <ion-title>{this.translations['title']}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <ion-header collapse="condense">
          <ion-toolbar>
            <ion-title size="large">{this.translations['title']}</ion-title>
          </ion-toolbar>
        </ion-header>
        <ion-refresher slot="fixed">
          <ion-refresher-content></ion-refresher-content>
        </ion-refresher>
        <div class="section-title">{this.translations['howAreYouDoingToday']}</div>

        {this.installButton ?
          <ion-card onClick={ev => this.installApp(ev)}>
            <ion-card-content>
              <p><ion-icon name="alert-circle-outline"></ion-icon> {this.translations['installApp.text']}</p>
            </ion-card-content>
          </ion-card>
          : ''}

        <ion-card class={"self-check " + (this.userData.riskModel.lastSymptomsCheck > 1 ? 'need-checking' : '')} href="/daily-check" routerDirection='forward'>
          <ion-card-header>
            <ion-card-subtitle>{this.translations['symptomSelfCheckup']}</ion-card-subtitle>
          </ion-card-header>
          <ion-card-content>
            <p innerHTML={this.userData.riskModel.lastSymptomsCheck > 1 ? this.translations['symptomSelfCheckup.checkUpNeeded'] : this.translations['symptomSelfCheckup.checkUpDone']}></p>
          </ion-card-content>
        </ion-card>

        <status-user userData={this.userData}></status-user>

        <div class="status-global">
          <div class="circle-progress-custom">
            <circle-progress progress-color="green" progress-amount={(this.globalData.risk*100) + ""}></circle-progress>
          </div>
          <span class={"indicator "+ this.globalData.riskType}>{this.globalData.risk * 100}% </span>

          {this.translations['totalPeopleFine']}
        </div>


        <div class="section-title">{this.translations['subTitle.latestInformations']}</div>
        {!!this.latestNews ? this.latestNews.map(news => <news-item news={news}></news-item>) : ''}
      </ion-content>
    ];
  }
}
