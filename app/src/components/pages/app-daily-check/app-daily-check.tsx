import {Component, Element, h, State} from '@stencil/core';
import {Symptoms, UserData} from "../../../service/model";
import {dataService} from "../../../service/data-service";
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: 'app-daily-check',
  styleUrl: 'app-daily-check.css'
})
export class AppNews {
  @State() userData: UserData;
  @State() symptoms: Symptoms;
  @Element() element: HTMLElement;

  translations: any;

  async componentWillLoad() {
    this.translations = await loadI18N(this.element);

    this.userData = await dataService.getUserData();
    this.userData = await dataService.recalculate(this.userData);

    if (this.userData.riskModel.lastSymptomsCheck > 1) {
      console.log("NEW CHECKING ENTITY days: ", this.userData.riskModel.lastSymptomsCheck);
      this.symptoms = {...this.userData.lastSymptoms}
      this.symptoms.date = new Date().getTime();
      // Initialize new state based on last check
    } else {
      this.symptoms = this.userData.lastSymptoms;
    }
  }

  saveData(ev) {
    console.log("updating daily symptoms", ev);
    dataService.saveUserData(this.userData, this.symptoms);
    // go back home
    document.querySelector("ion-router").back();
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/"/>
          </ion-buttons>
          <ion-title>Daily Check</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <div class="section-title" innerHTML={this.translations['summary']}></div>
        <symptom-check symptoms={this.symptoms} daily-check={true}></symptom-check>
      </ion-content>,
      <ion-footer>
        <ion-button onClick={ev => this.saveData(ev)} expand="block" color="primary"
                    size="large">{this.translations['button.save']}</ion-button>
      </ion-footer>
    ];
  }
}
