import {Component, Element, h} from "@stencil/core";
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: "app-tabs",
  styleUrl: "app-tabs.scss"
})
export class AppTabs {
  @Element() element: HTMLElement;
  translations : any;

  async componentWillLoad() {
    this.translations = await loadI18N(this.element);
  }

  render() {
    return [
      <ion-tabs>
        <ion-tab tab="tab-home">
          <ion-nav></ion-nav>
        </ion-tab>

        <ion-tab tab="tab-infos">
          <ion-nav></ion-nav>
        </ion-tab>

        <ion-tab tab="tab-sos">
          <ion-nav></ion-nav>
        </ion-tab>
        <ion-tab tab="tab-fake-news">
          <ion-nav></ion-nav>
        </ion-tab>

        <ion-tab-bar slot="bottom">
          <ion-tab-button tab="tab-home">
            <ion-icon name="heart-outline"></ion-icon>
            <ion-label>{this.translations['you']}</ion-label>
          </ion-tab-button>
          <ion-tab-button tab="tab-infos">
            <ion-icon name="information-circle-outline"></ion-icon>
            <ion-label>{this.translations['infos']}</ion-label>
          </ion-tab-button>
          <ion-tab-button tab="tab-sos">
            <ion-icon name="warning-outline"></ion-icon>
            <ion-label>{this.translations['sos']}</ion-label>
          </ion-tab-button>
          <ion-tab-button tab="tab-fake-news">
            <ion-icon name="flash-off-outline"></ion-icon>
            <ion-label>{this.translations['fakeNews']}</ion-label>
          </ion-tab-button>
        </ion-tab-bar>
      </ion-tabs>
    ];
  }
}
