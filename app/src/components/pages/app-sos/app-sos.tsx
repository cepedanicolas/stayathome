import {Component, Element, h} from '@stencil/core';
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: 'app-sos',
  styleUrl: 'app-sos.css'
})
export class AppSos {
  @Element() element: HTMLElement;
  translations : any;

  async componentWillLoad() {
    this.translations = await loadI18N(this.element);
  }


  render() {
    return [
      <ion-header translucent={true}>
        <ion-toolbar>
          <ion-title>{this.translations['title']}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <ion-header collapse="condense">
          <ion-toolbar>
            <ion-title size="large">{this.translations['title']}</ion-title>
          </ion-toolbar>
        </ion-header>

        <div class="section-title">{this.translations['subTitle']}</div>
        <p class="app-padding">{this.translations['intro']}</p>

        <ion-button href="tel:5551234567" color="danger" expand="full" size="large"><ion-icon name="nuclear-outline"></ion-icon> {this.translations['button.INeedHelp']}</ion-button>

        <p class="app-padding">{this.translations['closestHealthCare']}</p>
        <div class="maps">
        </div>
      </ion-content>
    ];
  }
}
