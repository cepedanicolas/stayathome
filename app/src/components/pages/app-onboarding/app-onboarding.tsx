import {Component, Element, h, Listen, Prop} from '@stencil/core';
import {UserData} from "../../../service/model";
import {dataService} from "../../../service/data-service";
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: 'app-onboarding',
  styleUrl: 'app-onboarding.css'
})
export class AppOnboarding {
  @Element() element: HTMLElement;

  @Prop() state: string = 'welcome';

  translations : any;

  userData: UserData;

  genderOptions: any[] = [{value: 'M', name: 'Male'}, {value: 'F', name: 'Female'}, {value: 'OTHER', name: 'Other'}];
  kantonOptions = [
    {value: 'AG', name: 'Aargau; Argovia'},
    {value: 'AI', name: 'Appenzell Innerrhoden; Appenzell Inner-Rhodes'},
    {value: 'AR', name: 'Appenzell Ausserrhoden; Appenzell Outer-Rhodes'},
    {value: 'BS', name: 'Basel-Stadt; Basle-City'},
    {value: 'BL', name: 'Basel-Landschaft; Basle-Country'},
    {value: 'BE', name: 'Bern; Berne'},
    {value: 'FR', name: 'Fribourg; Friburg[citation needed]'},
    {value: 'GE', name: 'Genève; Geneva'},
    {value: 'GL', name: 'Glarus; Glaris[citation needed]'},
    {value: 'GR', name: 'Graubünden; Grisons'},
    {value: 'JU', name: 'Jura'},
    {value: 'LU', name: 'Lucerne'},
    {value: 'NE', name: 'Neuchâtel'},
    {value: 'NW', name: 'Nidwalden; Nidwald[citation needed]'},
    {value: 'OW', name: 'Obwalden; Obwald[citation needed]'},
    {value: 'SH', name: 'Schaffhausen; Schaffhouse'},
    {value: 'SZ', name: 'Schwyz'},
    {value: 'SO', name: 'Solothurn; Soleure'},
    {value: 'SG', name: 'St. Gallen; St Gall'},
    {value: 'TG', name: 'Thurgau; Thurgovia'},
    {value: 'TI', name: 'Ticino; Tessin'},
    {value: 'UR', name: 'Uri'},
    {value: 'VS', name: 'Valais; Wallis'},
    {value: 'VD', name: 'Vaud'},
    {value: 'ZG', name: 'Zug; Zoug'},
    {value: 'ZH', name: 'Zürich; Zurich'}];

  exposureOptions = [{value: 1, name: 'Yes'}, {value: -1, name: 'No'}, {value: 2, name: 'Don\'t Know'}];

  async componentWillLoad() {
    this.userData = await dataService.getUserData();
    this.translations = await loadI18N(this.element);
    this.validateButtons();
  }

  @Listen('ionChange')
  ionChange(ev) {
    let propName = ev.target.name;

    let userDataProp = this.userData;

    if (propName.indexOf(".") > 0) {
      let subPropName = propName.substr(0, propName.indexOf("."));
      propName = propName.substr(propName.indexOf(".") + 1, propName.length);
      userDataProp = userDataProp[subPropName];
    }

    if(userDataProp.hasOwnProperty(propName)){
      // Check if property exists
      if (ev.target.tagName === 'ION-TOGGLE') {
        userDataProp[propName] = !!ev.detail.checked;
      } else {
        userDataProp[propName] = isNaN(ev.detail.value) ? ev.detail.value : parseInt(ev.detail.value);
      }
    }
    this.validateButtons();
  }

  @Prop({reflectToAttr: true, mutable: true, attribute: 'disabled'})
  data1ButtonEnabled: boolean = true;

  @Prop({reflectToAttr: true, mutable: true, attribute: 'disabled'})
  data3ButtonEnabled: boolean = true;

  @Prop({reflectToAttr: true, mutable: true, attribute: 'disabled'})
  data0ButtonEnabled: boolean = true;

  validateButtons() {
    // TODO: Validate Buttons are still right

    this.data0ButtonEnabled = true;
    this.data1ButtonEnabled = true;
    this.data3ButtonEnabled = true;

    /** Ignore validation
    this.data1ButtonEnabled = !!this.userData.basicData.gender && !!this.userData.basicData.kanton;
    this.data3ButtonEnabled = !!this.userData.quarantineConditions.daysInQuarantine && !!this.userData.quarantineConditions.householdSize && !!this.userData.quarantineConditions.contactWithPositiveCorona;

    this.data0ButtonEnabled = !dataService.hasPositiveSymptoms(this.userData.lastSymptoms) || !!this.userData.lastSymptoms.lastDaysWithSymptom;
     **/
  }

  renderWelcome() {
    return [
      <ion-content class="ion-padding">
        <img src="/assets/onboarding/start.png"/>
        <h1>{this.translations['welcome.title']}</h1>
        <p>{this.translations['welcome.subTitle']}</p>
        <p innerHTML={this.translations['welcome.disclaimer']}></p>
      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button href="/onboarding/intro1" expand="block" color="primary" size="large">{this.translations['button.start']}</ion-button>
      </ion-footer>
    ];
  }

  renderIntro1() {
    return [
      <ion-content class="ion-padding">
        <div class="section-title">{this.translations['intro1.title']}</div>
        <img src="/assets/onboarding/flattenTheCurve.png"/>
        <span innerHTML={this.translations['intro1.text']}></span>
      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button href="/onboarding/intro2" expand="block" color="primary" size="large">{this.translations['button.next']}</ion-button>
      </ion-footer>
    ];
  }

  renderIntro2() {
    return [
      <ion-content class="ion-padding">
        <div class="section-title">{this.translations['intro2.title']}</div>
        <img src="/assets/onboarding/dataPrivacy.png"/>
        <span innerHTML={this.translations['intro2.text']}></span>
      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button href="/onboarding/data0" expand="block" color="primary" size="large">{this.translations['button.next']}</ion-button>
      </ion-footer>
    ];
  }

  renderData0() {
    // TODO: This should be its own component

    return [
      <ion-content class="ion-padding">
        <div class="section-title">{this.translations['data0.title']}</div>
        <span innerHTML={this.translations['data0.text']}></span>

        <symptom-check symptoms={this.userData.lastSymptoms}></symptom-check>
      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button disabled={!this.data0ButtonEnabled} href="/onboarding/data1" expand="block" color="primary" size="large">{this.translations['button.next']}</ion-button>
      </ion-footer>
    ];
  }


  renderData1() {
    let ageOptions = [{value: '10', name: '0-20'}, {value: '30', name: '20-40'}, {
      value: '50',
      name: '40-60'
    }, {value: '70', name: '60-80'}, {value: '80', name: '> 80'}]

    return [
      <ion-content class="ion-padding">
        <div class="section-title">{this.translations['data1.title']}</div>
        <span innerHTML={this.translations['data1.text']}></span>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data1.age']}</ion-label>
          <ion-select name="basicData.age">
            {ageOptions.map(item =>
              <ion-select-option value={item.value}>{item.name}</ion-select-option>
            )}
          </ion-select>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data1.gender']}</ion-label>
          <ion-select name="basicData.gender">
            {this.genderOptions.map(item =>
              <ion-select-option value={item.value}>{item.name}</ion-select-option>
            )}
          </ion-select>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data1.kanton']}</ion-label>
          <ion-select name="basicData.kanton">
            {this.kantonOptions.map(item =>
              <ion-select-option value={item.value}>{item.name}</ion-select-option>)}
          </ion-select>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data1.doYouWorkWithOlderPeople']}</ion-label>
          <ion-toggle name="basicData.doYouWorkWithOlderPeople" slot="end"></ion-toggle>
        </ion-item>
      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button disabled={!this.data1ButtonEnabled} href="/onboarding/data2" expand="block" color="primary" size="large">{this.translations['button.next']}
        </ion-button>
      </ion-footer>
    ];
  }

  renderData2() {
    let formValues = ['healthConditions.highBloodPressure', 'healthConditions.diabetes', 'healthConditions.cardiovascularDisease', 'healthConditions.respiratoryDisease', 'healthConditions.weakImmuneSystem', 'healthConditions.asma', 'healthConditions.smoker'];

    return [
      <ion-content class="ion-padding">
        <div class="section-title">{this.translations['data2.title']}</div>
        <span innerHTML={this.translations['data2.text']}></span>

        {formValues.map(prop =>
          <ion-item>
            <ion-label class="ion-text-wrap">{this.translations['data2.' +prop]}</ion-label>
            <ion-toggle name={prop} slot="end"></ion-toggle>
          </ion-item>)}

      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button href="/onboarding/data3" expand="block" color="primary" size="large">{this.translations['button.next']}</ion-button>
      </ion-footer>
    ];
  }

  renderData3() {
    let daysInQuarantine = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20];
    let householdSize = [1, 2, 3, 4, 5, 6];
    return [
      <ion-content class="ion-padding">
        <div class="section-title">{this.translations['data3.title']}</div>
        <span innerHTML={this.translations['data3.text']}></span>

        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data3.quarantineConditions.daysInQuarantine']}</ion-label>
          <ion-select name="quarantineConditions.daysInQuarantine">
            {daysInQuarantine.map(item =>
              <ion-select-option value={item}>{item}</ion-select-option>)}
          </ion-select>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data3.quarantineConditions.householdSize']}</ion-label>
          <ion-select name="quarantineConditions.householdSize">
            {householdSize.map(item =>
              <ion-select-option value={item}>{item}</ion-select-option>)}
          </ion-select>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data3.quarantineConditions.workingFromHome']}</ion-label>
          <ion-toggle name="quarantineConditions.workingFromHome" slot="end"></ion-toggle>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data3.quarantineConditions.publicTransportation']}</ion-label>
          <ion-toggle name="quarantineConditions.publicTransportation" slot="end"></ion-toggle>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data3.quarantineConditions.travelInTheLastDays']}</ion-label>
          <ion-toggle name="quarantineConditions.travelInTheLastDays" slot="end"></ion-toggle>
        </ion-item>
        <ion-item>
          <ion-label class="ion-text-wrap">{this.translations['data1.contactWithPositiveCorona']}</ion-label>
          <ion-select name="quarantineConditions.contactWithPositiveCorona">
            {this.exposureOptions.map(item =>
              <ion-select-option value={item.value}>{item.name}</ion-select-option>)}
          </ion-select>
        </ion-item>
      </ion-content>,
      <ion-footer class="ion-padding">
        <ion-button disabled={!this.data3ButtonEnabled} href="/onboarding/done" expand="block" color="primary" size="large">{this.translations['button.next']}</ion-button>
      </ion-footer>
    ];
  }

  finish(){
    // Onboarding finished
    this.userData.basicData.onboarding = true;
    dataService.saveUserData(this.userData, this.userData.lastSymptoms);
    this.redirect();
  }

  redirect(){
    // Go to home
    //document.querySelector("ion-router").push('/home');
    window.location.href = "/home";
  }

  render() {
    if(this.userData.basicData.onboarding){
      this.redirect();
      return;
    }

    switch (this.state) {
      case 'welcome':
        return this.renderWelcome();
      case 'intro1' :
        return this.renderIntro1();
      case 'intro2' :
        return this.renderIntro2();
      case 'data0' :
        return this.renderData0();
      case 'data1' :
        return this.renderData1();
      case 'data2' :
        return this.renderData2();
      case 'data3' :
        return this.renderData3();
      case 'done':
        return this.finish();
    }
  }
}
