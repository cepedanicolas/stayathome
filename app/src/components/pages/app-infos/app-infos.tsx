import {Component, Element, h} from '@stencil/core';
import {News} from "../../../service/model";
import {loadI18N} from "../../../helpers/locale";
import {newsService} from "../../../service/news-service";

@Component({
  tag: 'app-infos',
  styleUrl: 'app-infos.css'
})
export class AppProfile {
  latestNews: News[];
  protectionActivities: News[];
  @Element() element: HTMLElement;
  translations : any;

  async componentWillLoad() {
    this.latestNews = await newsService.getLastestNews(2);
    this.protectionActivities = await newsService.getProtectionActivities();
    this.translations = await loadI18N(this.element);
  }

  render() {
    return [
      <ion-header translucent={true}>
        <ion-toolbar>
          <ion-title>{this.translations['title']}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true}>
        <ion-header collapse="condense">
          <ion-toolbar>
            <ion-title size="large">{this.translations['title']}</ion-title>
          </ion-toolbar>
        </ion-header>
        <div class="section-title">{this.translations['latestNews']}</div>
        {this.latestNews.map(news => <news-item news={news}></news-item>)}

        <div class="section-title">{this.translations['howToStaySafe']}</div>
        {this.protectionActivities.map(news => <news-item news={news}></news-item>)}
      </ion-content>
    ];
  }
}
