import {Component, h, Prop} from '@stencil/core';
import {News} from "../../../service/model";
import {newsService} from "../../../service/news-service";

@Component({
  tag: 'app-news',
  styleUrl: 'app-news.css'
})
export class AppNews {
  @Prop() newsId: string;
  @Prop() newsType: string;

  news: News;

  async componentWillLoad() {
    this.newsId = decodeURIComponent(this.newsId)
    this.news = await newsService.findNewsById(this.newsId + '', this.newsType);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/"/>
          </ion-buttons>
          <ion-title>News</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content fullscreen={true} class={"ion-padding news-item-" +this.news.type.toLowerCase()}>
        <news-item news={this.news} size="full-page"></news-item>
      </ion-content>
    ];
  }
}
