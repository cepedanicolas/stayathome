import {Component, Element, h, Listen, Prop, State} from '@stencil/core';
import {Symptoms} from '../../../service/model';
import {dataService} from "../../../service/data-service";
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: 'symptom-check',
  styleUrl: 'symptom-check.css'
})
export class SymptomCheck {
  @Prop() symptoms: Symptoms;
  @Prop() dailyCheck: boolean = false;

  @Element() element: HTMLElement;
  translations: any;

  @State() showLastDaysWithoutSymptoms: boolean = false;

  async componentWillLoad() {
    this.translations = await loadI18N(this.element);
  }

  @Listen('ionChange')
  ionChange(ev) {
    let propName = ev.target.name;

    if (ev.target.tagName === 'ION-TOGGLE') {
      this.symptoms[propName] = !!ev.detail.checked;
    } else {
      this.symptoms[propName] = isNaN(ev.detail.value) ? ev.detail.value : parseInt(ev.detail.value);
    }
    this.showLastDaysWithoutSymptoms = dataService.hasPositiveSymptoms(this.symptoms);
  }

  render() {
    let formValues = ['testedPositivCovid', 'fever', 'cough', 'shortnessOfBreath', 'headache', 'diarrea']
    let numberOfDays = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30];

    return [
      <div>
        {formValues.map(prop =>
          <ion-item>
            <ion-label class="ion-text-wrap">{this.translations['symptoms.' + prop]}</ion-label>
            <ion-toggle name={prop} slot="end" checked={!!this.symptoms[prop]}></ion-toggle>
          </ion-item>)
        }
        {
          this.showLastDaysWithoutSymptoms && !this.dailyCheck ? <ion-item>
            <ion-label class="ion-text-wrap">{this.translations['symptoms.date']}</ion-label>
            <ion-select name="lastDaysWithSymptom" value={this.symptoms.date}>
              {numberOfDays.map(item =>
                <ion-select-option value={item}>{item}</ion-select-option>
              )}
            </ion-select>
          </ion-item> : ''}
      </div>
    ]
  }
}
