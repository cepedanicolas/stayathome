import {Component, h, Prop} from '@stencil/core';
import {News, NewsType} from '../../../service/model';

@Component({
  tag: 'news-item',
  styleUrl: 'news-item.css'
})
export class NewsItem {
  @Prop() news: News;
  @Prop() size: string = 'small'; // 'small' 'full-page'

  renderArticle() {
    if (this.size === 'small') {
      return [
        <ion-card class='news-item news-item-article'
                  href={'/news/' + encodeURIComponent(this.news.id) + '/' + this.news.type}
                  routerDirection='forward'>
          <ion-card-header>
            <ion-card-subtitle> {this.news.title}</ion-card-subtitle>
          </ion-card-header>
          <ion-card-content>
            <img class="bag-logo" src="https://www.bag.admin.ch/etc/designs/core/frontend/guidelines/img/swiss.svg"
                 alt="Bundesamt für Gesundheit"/>
            <p innerHTML={this.news.summary}></p>
            <span class='date'>{this.news.date}</span>
          </ion-card-content>
        </ion-card>
      ];
    }
    // Else render Full Page View
    let showIframe = false;
    return [
      <div class="news-item-full news-item-article">
        {!!showIframe ?
          <iframe src={this.news.url}></iframe>
          : <div>
            <h1>{this.news.title}</h1>
            <img class="bag-logo" src="https://www.bag.admin.ch/etc/designs/core/frontend/guidelines/img/swiss.svg"
                 alt="Bundesamt für Gesundheit"/>
            <p>{this.news.content || this.news.summary}</p>
            <span class="date">{this.news.date}</span>
          </div>
        }
      </div>
    ]
  }

  renderFakeNews() {
    if (this.size === 'small') {
      return [
        <ion-card class='news-item news-item-fake-news'
                  href={'/news/' + encodeURIComponent(this.news.id) + '/' + this.news.type}
                  routerDirection='forward'>
          <ion-card-header>
            <ion-card-subtitle> {this.news.title}</ion-card-subtitle>
          </ion-card-header>
          <ion-card-content>
            <p innerHTML={this.news.summary}></p>
            <span class='date'>{this.news.date}</span>
          </ion-card-content>
        </ion-card>
      ];
    }

    // Else render Full Page View
    return [
      <div class="news-item-full news-item-fake-news">
        <h1>{this.news.title}</h1>
        <p innerHTML={this.news.content || this.news.summary}></p>
        <span class="date">{this.news.date}</span>
      </div>
    ]


  }

  renderProtectionActivity() {
    return [
      <ion-card class='news-item news-item-protection-activity'>
        <ion-card-header>
          <ion-card-subtitle>
            {this.news.title}
          </ion-card-subtitle>
        </ion-card-header>
        <ion-card-content>
          <img src={'/assets/protectionActivities/' + this.news.id + '.png'}/>
          <p innerHTML={this.news.summary}></p>
        </ion-card-content>
      </ion-card>
    ];

  }


  render() {
    switch (this.news.type) {
      case NewsType.Article:
        return this.renderArticle();
      case NewsType.FakeNews :
        return this.renderFakeNews();
      case NewsType.ProtectionActivity :
        return this.renderProtectionActivity();
    }
  }
}
