import {Component, Element, h, Prop} from '@stencil/core';
import {UserData} from "../../../service/model";
import {loadI18N} from "../../../helpers/locale";

@Component({
  tag: 'status-user',
  styleUrl: 'status-user.css'
})
export class StatusUser {
  @Prop() userData: UserData;

  @Element() element: HTMLElement;
  translations : any;

  async componentWillLoad() {
    this.translations = await loadI18N(this.element);
  }

  render() {
    return [
      <ion-card class={'status-user ' + this.userData.riskModel.riskType}>
        <ion-card-header>
          <ion-card-title>
            {this.translations['title.' + this.userData.riskModel.riskType]}
          </ion-card-title>
        </ion-card-header>
        <ion-card-content>
          <ion-label>
            {this.translations['tryToRemainInQuarantine']}
          </ion-label>
          <img src={"/assets/status/" + this.userData.riskModel.riskType + ".png"}/>
        </ion-card-content>
      </ion-card>
    ];
  }
}
