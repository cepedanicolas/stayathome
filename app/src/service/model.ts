export enum RiskType {
  danger = "danger",
  warning = "warning",
  ok = "ok",
  inmune = "inmune"
}

export interface UserData {
  id : string;

  schemaVersion : number;

  riskModel : RiskModel;

  basicData : BasicData;
  healthConditions : HealthConditions;
  quarantineConditions : QuarantineConditions
  lastSymptoms : Symptoms;
  symptomsHistory : [Symptoms];
}

export interface RiskModel {
  inmune : boolean;
  lastSymptomsCheck : number,
  lastDaysWithSymptoms : number;
  risk : number;
  riskType : RiskType;
}

export interface BasicData {
  age : number;
  gender : string;
  kanton : string;
  doYouWorkWithOlderPeople : boolean;

  onboarding : boolean;
  onboardingDate : number;
}

export interface Symptoms {
  testedPositivCovid : boolean;
  fever : boolean;
  cough : boolean;
  shortnessOfBreath : boolean;
  headache : boolean;
  diarrea : boolean;
  date : number;

  lastDaysWithSymptom : number;
}

export interface QuarantineConditions {
  daysInQuarantine : number;
  householdSize : number;
  workingFromHome : boolean;
  publicTransportation : boolean;
  travelInTheLastDays : boolean;
  date : number;

  contactWithPositiveCorona : number;
}

export interface HealthConditions {
  highBloodPressure : boolean;
  diabetes : boolean;
  cardiovascularDisease : boolean;
  respiratoryDisease : boolean;
  weakImmuneSystem : boolean;
  asma : boolean;
  smoker : boolean;
}

export interface GlobalData {
  risk : number,
  riskType : RiskType
}

export enum NewsType {
  Article = "Article",
  ProtectionActivity = "ProtectionActivity",
  FakeNews = "FakeNews"
}

export interface News {
  id : string;
  type : NewsType;
  title : string;
  summary?: string;
  content?: string;
  date ?: string;
  url ?: string;
}
