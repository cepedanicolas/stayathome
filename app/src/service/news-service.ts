import {News, NewsType} from "./model";

export class NewsService {
  cachedNews: News[] = [];

  async cacheNews(): Promise<News[]> {
    if (this.cachedNews.length > 0) {
      return this.cachedNews;
    }
    //let bagNewsResponse = await fetch('https://us-central1-letsstayathome-96e39.cloudfunctions.net/api/bagNews/', {cache: "no-cache"});
    let storedNewsResponse = await fetch("/assets/data/news.json", {cache: "no-cache"});

    this.cachedNews = [];
    //this.cachedNews = this.cachedNews.concat(await bagNewsResponse.json());
    this.cachedNews = this.cachedNews.concat(await storedNewsResponse.json());
    console.log("cached news: ", this.cachedNews);
    return this.cachedNews;
  }

  async emptyNewsCache() : Promise<News[]>{
    this.cachedNews = [];
    await this.cacheNews();
    return this.cachedNews;
  }

  async getLastestNews(max): Promise<News[]> {
    let news = await this.cacheNews();
    return news.filter(news => news.type == NewsType.Article).slice(0, max > news.length ? news.length : max);
  }

  async findNewsById(id, type): Promise<News> {
    let news = await this.cacheNews();
    return news.find(news => news.id === id && news.type === type);
  }

  async getProtectionActivities(): Promise<News[]> {
    let news = await this.cacheNews();
    return news.filter(news => news.type == NewsType.ProtectionActivity);
  }

  async getFakeNews(): Promise<News[]> {
    let news = await this.cacheNews();
    return news.filter(news => news.type == NewsType.FakeNews);
  }
}

export const newsService = new NewsService();
