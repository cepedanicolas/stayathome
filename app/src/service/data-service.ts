import {GlobalData, RiskType, Symptoms, UserData} from "./model";
import {storageService} from "./storage-service";

declare const firestore;

export class DataService {
  generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now() * 1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16;//random number between 0 and 16
      if (d > 0) {//Use timestamp until depleted
        r = (d + r) % 16 | 0;
        d = Math.floor(d / 16);
      } else {//Use microseconds since page-load if supported
        r = (d2 + r) % 16 | 0;
        d2 = Math.floor(d2 / 16);
      }
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  initialUserDataState: UserData = {
    id: this.generateUUID(),
    schemaVersion : 1, // Current schema version
    riskModel: {
      risk: 0.7,
      riskType: RiskType.danger,
      inmune: false,
      lastDaysWithSymptoms: -1,
      lastSymptomsCheck: 100, // autocalculated
    },
    basicData: {
      onboarding: false,
      onboardingDate: new Date().getTime(),
      age: undefined,
      gender: undefined,
      kanton: undefined,
      doYouWorkWithOlderPeople: false
    },
    healthConditions: {
      highBloodPressure: false,
      diabetes: false,
      cardiovascularDisease: false,
      respiratoryDisease: false,
      weakImmuneSystem: false,
      asma: false,
      smoker: false
    },
    quarantineConditions: {
      daysInQuarantine: 0,
      householdSize: 0,
      workingFromHome: false,
      publicTransportation: false,
      travelInTheLastDays: true,
      contactWithPositiveCorona: undefined,
      date: new Date().getTime()
    },
    lastSymptoms: {
      fever: false,
      cough: false,
      shortnessOfBreath: false,
      headache: false,
      diarrea: false,
      testedPositivCovid: false,
      date: new Date().getTime(),
      lastDaysWithSymptom: undefined
    },
    symptomsHistory: <any>[]
  };

  mockGlobalData: GlobalData = {
    risk: 0.6,
    riskType: RiskType.ok
  }

  async getUserData(): Promise<UserData> {
    let userData: UserData = await storageService.fetchUserData();
    if (!!userData) {
      console.log("Initializing with state: ", userData)
      userData = await this.recalculate(userData);
      return userData;
    }
    console.log("No initial user state");
    return this.initialUserDataState;
  }

  async saveUserData(userData: UserData, symptoms ?: Symptoms): Promise<UserData> {
    console.log("Storing state: ", userData);
    // We have new symptoms to be stored
    if (!!symptoms) {
      // we have either a new one or the history is empty.
      if (userData.symptomsHistory.length <= 0 ||
        (userData.symptomsHistory.length > 0 &&
          userData.symptomsHistory[userData.symptomsHistory.length - 1].date < symptoms.date)) {
        console.log("PUSHING NEW SYMPTOMS", symptoms);
        // Store latest lastSymptoms
        userData.lastSymptoms = symptoms;
        userData.symptomsHistory.push(symptoms);
      }
    }
    userData = await this.recalculate(userData);
    await storageService.storeUserData(userData);

    console.log("storing to fire base");
    console.log("storing: ", userData);
    firestore.collection('user').doc(userData.id).set(userData);

    return userData;
  }

  async recalculate(userData: UserData): Promise<UserData> {
    let risk = 0; // By default we assume no risk


    if (userData.healthConditions.highBloodPressure ||
      userData.healthConditions.diabetes ||
      userData.healthConditions.cardiovascularDisease ||
      userData.healthConditions.respiratoryDisease ||
      userData.healthConditions.weakImmuneSystem ||
      userData.healthConditions.asma ||
      userData.healthConditions.smoker) {
      risk = 1; // High risk group
    }

    // Corona:
    // 2 - Träger & hohne symptome
    // 4 - Träger & Symptome
    // 6 - Symptome

    // contactWithPositiveCorona, 0 = NO, 1 = Yes, 2 = I don't Know
    // TODO: Time Decay exponential decay for 14 Tage
    if (userData.quarantineConditions.contactWithPositiveCorona === 1) {
      // Yes
      risk += 0.70; // High risk of contagious
    } else if (userData.quarantineConditions.contactWithPositiveCorona === 2) {
      // Don't really know
      risk += 0.40; // High risk of contagious
    }

    if (userData.basicData.doYouWorkWithOlderPeople) {
      risk = 1; // High risk group due to exposure
    }

    // Quarantine only helps if risk < 0.8, otherwise remain like that
    if (userData.quarantineConditions.householdSize > 0) {
      // the bigger the household the higher the exponential social group
      risk += userData.quarantineConditions.householdSize * 0.2;
    }

    if (!userData.quarantineConditions.workingFromHome) {
      // Not working from home is risky
      risk += 0.1;
    }

    if (userData.quarantineConditions.publicTransportation) {
      // Public Transportation is risky
      risk += 0.1;
    }

    if (!userData.quarantineConditions.travelInTheLastDays) {
      // Traveling is risky
      risk += 0.3;
    }

    // Now lets take the first positive recorded symptoms into consideration
    // TODO: find latest positive symptoms
    let symptoms: Symptoms = userData.lastSymptoms;
    userData.riskModel.lastDaysWithSymptoms = 100; // Not yet defined
    userData.riskModel.inmune = false; // We assume he is inmune

    // TODO: the more symptoms the higher warhscheinlichkeit

    if (!!symptoms) {
      let milisecondsPerDay = 60 * 60 * 24 * 1000;
      if (!!symptoms.lastDaysWithSymptom) {
        let lastSymptomDate = symptoms.date - milisecondsPerDay * symptoms.lastDaysWithSymptom; // go back in time to calculate last date of infection
        userData.riskModel.lastDaysWithSymptoms = (new Date().getTime() - lastSymptomDate) / milisecondsPerDay;
      }
      userData.riskModel.lastSymptomsCheck = (new Date().getTime() - symptoms.date) / milisecondsPerDay;
    }

    // TODO: Remove this line debugging only
    // userData.lastSymptomsCheck = 10;

    // Had positive symptoms
    if (this.hasPositiveSymptoms(symptoms)) {
      if (userData.riskModel.lastDaysWithSymptoms < 0) {
        // We don't know yet
      } else if (userData.riskModel.lastDaysWithSymptoms < 6) {
        // High risk of infection
        console.log("High risk of infection due to symptoms in the last 3-5 days");
        userData.riskModel.risk = 1;
      } else {
        console.log("User had the symptoms more than 6 days ago, should be inmune now");
        userData.riskModel.risk = 0.1;
        userData.riskModel.inmune = true;
      }
    }

    // TODO: Add also daily symptom tracking into this formula

    userData.riskModel.risk = risk;

    // Sanity checks for now
    if (userData.riskModel.risk > 1) {
      userData.riskModel.risk = 1;
    }
    if (userData.riskModel.risk < 0) {
      userData.riskModel.risk = 0;
    }

    if (userData.riskModel.inmune) {
      userData.riskModel.riskType = RiskType.inmune;
    } else if (userData.riskModel.risk >= 0.66) {
      userData.riskModel.riskType = RiskType.danger;
    } else if (userData.riskModel.risk >= 0.33) {
      userData.riskModel.riskType = RiskType.warning;
    } else if (userData.riskModel.risk >= 0) {
      userData.riskModel.riskType = RiskType.ok;
    }

    console.log("Finshing computing risk: ", userData.riskModel.risk, " data:", userData);

    return userData;
  }

  hasPositiveSymptoms(symptoms: Symptoms): boolean {
    // TODO: at least two or shortnessOfBreath
    return !!symptoms.fever ||
      !!symptoms.cough ||
      !!symptoms.diarrea ||
      !!symptoms.headache ||
      !!symptoms.shortnessOfBreath;
  }

  async getGlobalData(): Promise<GlobalData> {
    return this.mockGlobalData;
  }
}

export const dataService = new DataService();
