import {UserData} from "./model";

export class StorageService {
  async fetchUserData() : Promise<UserData> {
    let data =window.localStorage.getItem('userData');
    if(!!data){ //&& data.hasOwnProperty('schemaVersion') && data['schemaVersion'] >= 1){
      return JSON.parse(data);
    }
    return undefined;
  }

  async storeUserData(userData:  UserData) : Promise<UserData> {
    window.localStorage.setItem('userData', JSON.stringify(userData));
    return userData;
  }
}

export const storageService = new StorageService();
